#include "stdafx.h"
#include "Utility.h"

QImage Utility::Mat2QImage(cv::Mat src)
{
	cv::cvtColor(src, src, cv::COLOR_BGR2RGB);
	QImage dest = QImage(src.data, src.cols, src.rows, src.step, QImage::Format_RGB888);
	return dest;
}
void Utility::updateImg(cv::Mat cvimg, QLabel *lable)
{
	if (!cvimg.empty())
	{
		QImage img = Utility::Mat2QImage(cvimg);
		int height = (float)lable->width()*((float)img.height() / (float)img.width());
		lable->setFixedHeight(height);
		lable->setPixmap(QPixmap::fromImage(img).scaled(lable->width(), height, Qt::KeepAspectRatio));
		lable->setScaledContents(true);
		lable->show();
	}
}