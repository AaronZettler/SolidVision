/**
 * @file	Utility.h.
 *
 * @brief	Declares the utility class
 */
#pragma once
#include<opencv2\opencv.hpp>
#include <QtGui>
/**
 * @class	Utility
 *
 * @brief	Class for utility functions
 */
class Utility
{
public:

	/**
	 * @fn	static QImage Utility::Mat2QImage(cv::Mat src);
	 *
	 * @brief	Converts a cv Matrix to a QT Image 
	 *
	 * @param	src cv:Mat 
	 *
	 * @returns	A QImage.
	 */
	static QImage Mat2QImage(cv::Mat src);
	/**
	 * @fn	static void Utility::updateImg(cv::Mat cvimg, QLabel *lable);
	 *
	 * @brief	Updates an Image, displays the image in a QT-Lable
	 *
	 * @param 		  	cvimg	The cv:Mat img.
	 * @param [in,out]	lable	If non-null, the lable.
	 */
	static void updateImg(cv::Mat cvimg, QLabel *lable);
};