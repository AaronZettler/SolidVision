/**
 * @file	SolidVision.h.
 *
 * @brief	Declares the solid vision class
 */
#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_SolidVision.h"
#include "Calibration.h"
#include <qtimer>
#include <filesystem>

#include "CameraController.h"
#include "DepthmapGenerator.h"
#include "ObjectTracker.h"
#include "ColorTracker.h"
#include "SerialController.h"

/**
 * @class	SolidVision
 *
 * @brief	The main QT-Window
 */
class SolidVision : public QMainWindow
{
	Q_OBJECT

public:

	/**
	 * @fn	SolidVision::SolidVision(QWidget *parent = Q_NULLPTR);
	 *
	 * @brief	Constructor
	 *
	 * @param [in,out]	parent	(Optional) If non-null, the parent.
	 */
	SolidVision(QWidget *parent = Q_NULLPTR);
	/**
	 * @fn	SolidVision::~SolidVision();
	 *
	 * @brief	Destructor
	 */
	~SolidVision();
	QTimer *timer;  ///< The timer
	QTimer *serialtimer;	///< The serial-communication timer

public slots:
	/**
	 * @fn	void SolidVision::on_pushButtonGetCameras_clicked();
	 *
	 * @brief	Handles push button get cameras clicked signals
	 */
	void on_pushButtonGetCameras_clicked();
	/**
	 * @fn	void SolidVision::on_pushButtonInputL_clicked();
	 *
	 * @brief	Handles push button input l clicked signals
	 */
	void on_pushButtonInputL_clicked();
	/**
	 * @fn	void SolidVision::on_pushButtonInputR_clicked();
	 *
	 * @brief	Handles push button input r clicked signals
	 */
	void on_pushButtonInputR_clicked();
	/**
	 * @fn	void SolidVision::on_pushButtonStartFromFile_clicked();
	 *
	 * @brief	Handles push button start from file clicked signals
	 */
	void on_pushButtonStartFromFile_clicked();
	/**
	 * @fn	void SolidVision::on_pushButtonGetCalibration_clicked();
	 *
	 * @brief	Handles push button get calibration clicked signals
	 */
	void on_pushButtonGetCalibration_clicked();
	/**
	 * @fn	void SolidVision::on_pushButtonUseFile_clicked();
	 *
	 * @brief	Handles push button use file clicked signals
	 */
	void on_pushButtonUseFile_clicked();
	/**
	 * @fn	void SolidVision::on_pushButtonGetMargins_clicked();
	 *
	 * @brief	Handles push button get margins clicked signals
	 */
	void on_pushButtonGetMargins_clicked();
	/**
	 * @fn	void SolidVision::on_checkBoxDepthMapBig_clicked(bool checked);
	 *
	 * @brief	Handles check box depth map big clicked signals
	 *
	 * @param	checked	True if checked.
	 */
	void on_checkBoxDepthMapBig_clicked(bool checked);
	/**
	 * @fn	void SolidVision::on_checkBoxSerial_clicked(bool checked);
	 *
	 * @brief	Handles check box serial clicked signals
	 *
	 * @param	checked	True if checked.
	 */
	void on_checkBoxSerial_clicked(bool checked);
	/**
	 * @fn	void SolidVision::on_comboBoxLeftCam_currentIndexChanged(const QString &text);
	 *
	 * @brief	Handles combo box left camera current index changed signals
	 *
	 * @param	text	The text.
	 */
	void on_comboBoxLeftCam_currentIndexChanged(const QString &text);
	/**
	 * @fn	void SolidVision::on_comboBoxRightCam_currentIndexChanged(const QString &text);
	 *
	 * @brief	Handles combo box right camera current index changed signals
	 *
	 * @param	text	The text.
	 */
	void on_comboBoxRightCam_currentIndexChanged(const QString &text);
	/**
	 * @fn	void SolidVision::on_comboBoxDepthMapMode_currentIndexChanged(const QString &text);
	 *
	 * @brief	Handles combo box depth map mode current index changed signals
	 *
	 * @param	text	The text.
	 */
	void on_comboBoxDepthMapMode_currentIndexChanged(const QString &text);
	/**
	 * @fn	void SolidVision::on_comboBoxSerialPort_currentIndexChanged(const QString &text);
	 *
	 * @brief	Handles combo box serial port current index changed signals
	 *
	 * @param	text	The text.
	 */
	void on_comboBoxSerialPort_currentIndexChanged(const QString &text);
	/**
	 * @fn	void SolidVision::timerTick();
	 *
	 * @brief	Handles timer tick signals
	 */
	void timerTick();
	/**
	 * @fn	void SolidVision::serialtimerTick();
	 *
	 * @brief	Handles serialtimer tick signals
	 */
	void serialtimerTick();


private:

	/**
	 * @fn	void SolidVision::timerInit();
	 *
	 * @brief	Timer initialize
	 */
	void timerInit();
	/**
	 * @fn	void SolidVision::serialtimerInit();
	 *
	 * @brief	Serialtimer initialize
	 */
	void serialtimerInit();
	/**
	 * @fn	QString SolidVision::getCurrentDirectory();
	 *
	 * @brief	Gets current directory
	 *
	 * @returns	The current directory.
	 */
	QString getCurrentDirectory();
	/**
	 * @fn	QString SolidVision::getFile(QString defaultDirectory, QString filter);
	 *
	 * @brief	Gets a file from a specifiable directory
	 *
	 * @param	defaultDirectory	The default directory.
	 * @param	filter				Specifies the filter.
	 *
	 * @returns	The file.
	 */
	QString getFile(QString defaultDirectory, QString filter);
	/**
	 * @fn	void SolidVision::setMode(QString mode);
	 *
	 * @brief	Sets a mode (representation of the depth-map in grayscale or colors)
	 *
	 * @param	mode	The mode.
	 */
	void setMode(QString mode);
	/**
	 * @fn	void SolidVision::getCameras();
	 *
	 * @brief	Gets the current frame from both video-inputs when available, tests if input-source exists
	 */
	void getCameras();
	/**
	 * @fn	void SolidVision::showCameras();
	 *
	 * @brief	Displays the current frame in QT
	 */
	void showCameras();

	Ui::SolidVisionClass* ui;   ///< The user interface
	Calibration* calibration;   ///< The calibration
	SerialController::Controller serialcontroller;  ///< The serialcontroller

	CameraControl::Controller camcontroller;	///< The camcontroller
	StereoVision3D::DepthmapGenerator depthmapgenerator;	///< The depthmapgenerator
	ObjectTracking::CascadeTracker tracker; ///< The tracker
	ObjectTracking::ColorMargins currentmargins;	///< The currentmargins (measured for every frame in the image-center)
	ObjectTracking::ColorMargins margins;   ///< The margins (margins used by the ColorTracker)
	int idCamL; ///< The identifier camera L
	int idCamR; ///< The identifier camera R
	cv::Mat camL;   ///< The camera L
	cv::Mat	camR;   ///< The camera R
	QString fromfileL;  ///< The vide-input from a file L
	QString fromfileR;  ///< The vide-input from a file R
	StereoVision3D::Calibrator calibratorL; ///< The calibrator L
	StereoVision3D::Calibrator calibratorR; ///< The calibrator R
	cv::Point position; ///< The position of the tracked object
	cv::Vec3f measurement;  ///< The measurement at the object-position
	int measure = 0;	///< The measurement at the object-position in only the z-axis (distence from the camera) 
	StereoVision3D::StereoCalibrationData stereocalibrationdata;	///< The stereocalibrationdata
};
