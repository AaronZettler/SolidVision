#include "stdafx.h"
#include "SolidVision.h"

#include <qstringlist.h>
#include "Utility.h"
#include <QLabel>

#define OBJECT_CASCAD_PATH "cascade.xml"
#define FROM_FILE_CAML_ID 0
#define FROM_FILE_CAMR_ID 1

#define VIDEO_FILE_FILTER "Video Files (*.avi)"
#define CALIBRATION_FILE_FILTER "Calibration (*.xml)"

#define MEASURE_ROW 640/2
#define MEASURE_COL 480/2

#define NOPORT "None"

SolidVision::SolidVision(QWidget *parent)
	: QMainWindow(parent),
	ui(new Ui::SolidVisionClass)
{
	ui->setupUi(this);
	timerInit();
	serialtimerInit();
	tracker.initTracker(OBJECT_CASCAD_PATH);
	std::string str = QT_VERSION_STR;
}
SolidVision::~SolidVision()
{
	serialcontroller.disconnectFromDevice();
}

//Slots
void SolidVision::on_pushButtonGetCameras_clicked()
{
	int i = 0;

	//Get Cameras
	std::vector<int> camIdx;
	if (camcontroller.getCameraIds(&camIdx))
	{
		for (int i = 0; i < camIdx.size(); i++)
			std::cout << "ID: " << std::to_string(camIdx[i]) << std::endl;
	}
	else
	{
		std::cout << "No Cameras found" << std::endl;
	}

	//Add Cameras
	QStringList camids;
	for (auto id : camIdx)
	{
		camids.append(QString::number(id));
	}
	ui->comboBoxLeftCam->clear();
	ui->comboBoxRightCam->clear();
	ui->comboBoxLeftCam->addItems(camids);
	ui->comboBoxRightCam->addItems(camids);
}
void SolidVision::on_pushButtonInputL_clicked()
{
	QString path = getFile(getCurrentDirectory(), VIDEO_FILE_FILTER);
	if (!path.isEmpty())
	{
		ui->labeFromFileL->setText(path.mid(path.length() - 20, 20));
		fromfileL = path;
	}
}
void SolidVision::on_pushButtonInputR_clicked()
{
	QString path = getFile(getCurrentDirectory(), VIDEO_FILE_FILTER);
	if (!path.isEmpty())
	{
		ui->labeFromFileR->setText(path.mid(path.length() - 20, 20));
		fromfileR = path;
	}
}
void SolidVision::on_pushButtonStartFromFile_clicked()
{
	camcontroller.clearSources();
	idCamL = FROM_FILE_CAML_ID;
	idCamR = FROM_FILE_CAMR_ID;
	camcontroller.addSource(FROM_FILE_CAML_ID, fromfileL.toStdString(), ui->checkBoxLoop->isChecked());
	camcontroller.addSource(FROM_FILE_CAMR_ID, fromfileR.toStdString(), ui->checkBoxLoop->isChecked());
}
void SolidVision::on_pushButtonGetCalibration_clicked()
{
	calibration = new Calibration(&camcontroller, idCamL, idCamR);

	timer->stop();
	calibration->exec();
	calibratorL = calibration->calibratorL;
	calibratorR = calibration->calibratorR;
	stereocalibrationdata = calibration->scd;
	timer->start();
}
void SolidVision::on_pushButtonUseFile_clicked()
{
	QString path = getFile(getCurrentDirectory(), CALIBRATION_FILE_FILTER);
	if (!path.isEmpty())
	{
		ui->labeCalibration->setText(path.mid(path.length() - 20, 20));
		calibratorL.loadCalibration(path.toStdString(), "L");
		calibratorR.loadCalibration(path.toStdString(), "R");
		stereocalibrationdata = StereoVision3D::Calibrator::loadStereoCalibration(path.toStdString(), "STEREO");
	}
}
void SolidVision::on_pushButtonGetMargins_clicked()
{
	margins = currentmargins;
}
void SolidVision::on_checkBoxDepthMapBig_clicked(bool checked)
{
	switch (checked)
	{
	case true:
		depthmapgenerator.createtWindow();
		break;
	case false:
		depthmapgenerator.destroyWindow();
		break;
	}
}
void SolidVision::on_comboBoxLeftCam_currentIndexChanged(const QString &text)
{
	int id = text.toInt();

	camcontroller.addSource(id, id);
	idCamL = id;
}
void SolidVision::on_comboBoxRightCam_currentIndexChanged(const QString &text)
{
	int id = text.toInt();

	camcontroller.addSource(id, id);
	idCamR = id;
}
void SolidVision::on_comboBoxDepthMapMode_currentIndexChanged(const QString &text)
{
	setMode(text);
}
void SolidVision::on_checkBoxSerial_clicked(bool checked)
{
	if (checked)
	{
		QList<QSerialPortInfo> info = serialcontroller.getDevices();
		QStringList portnames;

		portnames.append(NOPORT);
		for (auto port : info)
		{
			portnames.append(port.portName());
		}

		ui->comboBoxSerialPort->clear();
		ui->comboBoxSerialPort->addItems(portnames);
	}
}
void SolidVision::on_comboBoxSerialPort_currentIndexChanged(const QString &text)
{
	if (text == NOPORT)
		return;

	try
	{
		serialcontroller.disconnectFromDevice();
		serialcontroller.connectToDevice(text);
	}
	catch (const std::exception&)
	{
		ui->comboBoxSerialPort->setCurrentIndex(0);
	}
}
void SolidVision::timerTick()
{
	camcontroller.captureFrames();

	depthmapgenerator.newdispmode = ui->checkBoxDepthMapNew->isChecked();

	if ((camcontroller.sourceExists(idCamL) & camcontroller.sourceExists(idCamR)) ||						//2 Cameras exist
		camcontroller.sourceExists(FROM_FILE_CAML_ID) & camcontroller.sourceExists(FROM_FILE_CAMR_ID))	//2 From File Inputs exist
	{
		getCameras();

		if (calibratorL.ready && calibratorR.ready && !camL.empty() && !camR.empty())
		{
			/*camL = calibratorL.Undistort(camL);
			camR = calibratorR.Undistort(camR);*/
			StereoVision3D::StereoMat smat;
			smat = StereoVision3D::Calibrator::Undistort(camL, camR, stereocalibrationdata);
			camL = smat.imgL;
			camR = smat.imgR;
		}

		showCameras();
		

		if (ui->checkBoxObjectTracking->isChecked())
		{
			try
			{
				cv::Mat withObjects = camL.clone();
				if (ui->checkBoxColorTracking->isChecked())
				{
					currentmargins = ObjectTracking::ColorTracker::getMargins(withObjects, 40);
					position = ObjectTracking::ColorTracker::detectObject(withObjects, margins);

					//Draw 
					ObjectTracking::ColorTracker::drawDetectionPoint(withObjects, position, 50);
					ObjectTracking::ColorTracker::drawMarginMeasurePoint(withObjects, 6);
				}
				else
				{
					cv::Mat withObjects = camL.clone();
					tracker.detectObjects(withObjects);
					tracker.drawDetection(withObjects);
					position = tracker.position;
				}
				Utility::updateImg(withObjects, ui->ImgObjectTracking);
			}
			catch (const std::exception& e)
			{

			}
		}
		else
		{
			position = cv::Point(MEASURE_ROW, MEASURE_COL);
		}
		if (ui->checkBoxDepthMap->isChecked())
		{
			try
			{
				cv::Mat map;
				depthmapgenerator.generate(camL, camR);

				cv::Mat dmap = depthmapgenerator.getDepthMap();
				depthmapgenerator.setFilter(ui->horizontalSliderErode->value(),ui->horizontalSliderDialate->value(),StereoVision3D::FilterMode::BASIC);
				depthmapgenerator.filterMap(dmap);
				measurement = depthmapgenerator.getMeasurementAtPosition(position.y, position.x, stereocalibrationdata.Q);

				measure =(int) measurement[2]*3.33; //Z bzw Abstand
				if (measure > 1000 || measure < 0) measure = 0;

				cv::Mat mapnormalized = depthmapgenerator.getDepthMapNormalized();

				cv::cvtColor(mapnormalized, mapnormalized, cv::COLOR_GRAY2BGR);

				switch (depthmapgenerator.mode)
				{
				case StereoVision3D::Mode::NORMAL:
					map = mapnormalized;
					break;
				case StereoVision3D::Mode::COLOR:
					map = depthmapgenerator.getDepthMapColored(camL, mapnormalized);
					break;
				default:
					break;
				}

				depthmapgenerator.drawCrosshairs(map, position.x, position.y, 40, 2);

				//Show Map
				if (ui->checkBoxDepthMapBig->isChecked())
					depthmapgenerator.show(map);
				Utility::updateImg(map, ui->ImgDepthMap);


			}
			catch (const std::exception& e)
			{
				std::string str = e.what();
			}
		}
		ui->labelMeasurementValue->setText("X[" + QString::number(measurement[0]) + 
										 "] Y[" + QString::number(measurement[1]) + 
										 "] Z[" + QString::number(measurement[2]) + "]" + "Abstand:" + QString::number(measure));
	}
}
void SolidVision::serialtimerTick()
{
	if (ui->checkBoxSerial->isChecked() & serialcontroller.connected) //and if comunication is ready
	{
		try
		{
			serialcontroller.sendData("d", measure);
		}
		catch (const std::exception&)
		{

		}
	}
}

//Other
void SolidVision::timerInit()
{
	// create a timer
	timer = new QTimer(this);

	// setup signal and slot
	QTimer::connect(timer, SIGNAL(timeout()), this, SLOT(timerTick()));

	// msec
	timer->start(1000 / 30); //30fps
}
void SolidVision::serialtimerInit()
{
	// create a timer
	serialtimer = new QTimer(this);

	// setup signal and slot
	QTimer::connect(serialtimer, SIGNAL(timeout()), this, SLOT(serialtimerTick()));

	// msec
	serialtimer->start(100); //30fps
}
QString SolidVision::getCurrentDirectory()
{
	std::string str = std::experimental::filesystem::current_path().string();
	return QString::fromStdString(str);
}
QString SolidVision::getFile(QString defaultDirectory, QString filter)
{
	QFileDialog dialog(this);
	dialog.setFileMode(QFileDialog::ExistingFile);
	dialog.setNameFilter(filter);
	dialog.setDirectory(defaultDirectory);
	if (dialog.exec())
	{
		QStringList files = dialog.selectedFiles();
		return files[0];
	}
	return "";
}
void SolidVision::setMode(QString mode)
{
	if (mode == "Normal")
		depthmapgenerator.mode = StereoVision3D::Mode::NORMAL;
	else if (mode == "Color")
		depthmapgenerator.mode = StereoVision3D::Mode::COLOR;
}
void SolidVision::getCameras()
{
	if (camcontroller.sourceExists(idCamL))
		camL = camcontroller.getFrame(idCamL).clone();

	if (camcontroller.sourceExists(idCamR))
		camR = camcontroller.getFrame(idCamR).clone();
}
void SolidVision::showCameras()
{
	if (!camL.empty())
		Utility::updateImg(camL.clone(), ui->ImgLeft);

	if (!camR.empty())
		Utility::updateImg(camR.clone(), ui->ImgRight);
}
