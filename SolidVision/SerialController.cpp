#include "stdafx.h"
#include"SerialController.h"

#define TYPE_INT "tint"

namespace SerialController
{
	Controller::Controller()
	{
		//Init Port
		port.setBaudRate(9600);
		port.setDataBits(QSerialPort::Data8); //8 databits
		port.setParity(QSerialPort::NoParity);
		port.setStopBits(QSerialPort::OneStop);
		port.setFlowControl(QSerialPort::NoFlowControl);
	}

	QList<QSerialPortInfo> Controller::getDevices()
	{
		return QSerialPortInfo::availablePorts();
	}

	void Controller::connectToDevice(QString portinfo)
	{
		port.setPortName(portinfo);

		if (!port.open(QSerialPort::WriteOnly))
			throw std::exception("Could not open SerialPort");

		connected = true;
	}
	void Controller::sendData(QString data)
	{
		port.write(data.toUtf8());
	}
	void Controller::sendData(QByteArray data)
	{
		port.write(data);
	}
	void Controller::sendData(QString name, uint16_t data)
	{
		QByteArray ba(QString(TYPE_INT + name).toUtf8() + (char)data + (char)(data >> 8));
		sendData(ba);
	}
	void Controller::disconnectFromDevice()
	{
		port.close();
		connected = false;
	}
}