#include "stdafx.h"
#include "Calibration.h"
#include "Utility.h"
#include <QLabel>

#define BOARD_DETECTED calibratorL.found && calibratorR.found

Calibration::Calibration(CameraControl::Controller* cameracontroller, int idcamL, int idcamR, QDialog *parent) : QDialog(parent), ui(new Ui::Calibration)
{
	idCamL = idcamL;
	idCamR = idcamR;
	camcontroller = cameracontroller;
	calibratorL.init();
	calibratorR.init();
	ui->setupUi(this);
	timerInit();

	updateCapturedCount();
}

void Calibration::on_pushButtonCapture_clicked() 
{
	if (BOARD_DETECTED && capturedcount < calibratorL.numBoards)
	{
		calibratorL.useBoard();
		calibratorR.useBoard();

		calibratorL.saveBoard(CALIBRATION_DATA_PATH_LEFT);
		calibratorR.saveBoard(CALIBRATION_DATA_PATH_LEFT);

		capturedcount++;

		if (capturedcount == calibratorL.numBoards)
		{
			calibratorL.calibrateImg(1);
			calibratorR.calibrateImg(1);
			scd = StereoVision3D::Calibrator::calibrateStereoImg(calibratorL, calibratorR);


			calibratorL.saveCalibration("calibration.xml", "L", false);
			calibratorR.saveCalibration("calibration.xml", "R", true);
			StereoVision3D::Calibrator::saveStereoCalibration(scd, "calibration.xml", "STEREO", true);
		}

		updateCapturedCount();
	}
}
void Calibration::timerTick()
{
	camcontroller->captureFrames();

	//get Cameras
	camL = camcontroller->getFrame(idCamL).clone();
	camR = camcontroller->getFrame(idCamR).clone();

	//detect and display Boared
	calibratorL.detectBoard(camL);
	calibratorR.detectBoard(camR);
	setIndicatorBoard(BOARD_DETECTED);
	if (BOARD_DETECTED)
	{
		Utility::updateImg(calibratorL.getImgCorners().clone(), ui->ImgLeft);
		Utility::updateImg(calibratorR.getImgCorners().clone(), ui->ImgRight);
	}
	else
	{
		Utility::updateImg(camL.clone(), ui->ImgLeft);
		Utility::updateImg(camR.clone(), ui->ImgRight);
	}
}

void Calibration::timerInit()
{
	// create a timer
	timer = new QTimer(this);

	// setup signal and slot
	QTimer::connect(timer, SIGNAL(timeout()), this, SLOT(timerTick()));

	// msec
	timer->start(1000 / 30); //30fps
}
void Calibration::updateCapturedCount()
{
	ui->labelImagesCapturedCount->setText(QString::number(capturedcount) + "/" + QString::number(calibratorL.numBoards));
}
void Calibration::setIndicatorBoard(bool found)
{
	//Adapt
	ui->indicatorBoard->setFixedHeight(ui->ImgLeft->height());

	if (found)
		ui->indicatorBoard->setStyleSheet("QLabel { background-color : lightgreen}");
	else
		ui->indicatorBoard->setStyleSheet("QLabel { background-color : red}");
}