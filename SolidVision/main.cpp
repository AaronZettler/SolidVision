#include "stdafx.h"
#include "SolidVision.h"
#include <QtWidgets/QApplication>
/**
 * @fn	int main(int argc, char *argv[])
 *
 * @brief	Main entry-point for this application
 *
 * @param	argc	The number of command-line arguments provided.
 * @param	argv	An array of command-line argument strings.
 *
 * @returns	Exit-code for the process - 0 for success, else an error code.
 */
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	SolidVision w;
	w.show();
	return a.exec();
}
