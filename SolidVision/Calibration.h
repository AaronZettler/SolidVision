/**
 * @file	Calibration.h.
 *
 * @brief	Declares the calibration class
 */
#pragma once

#include <QtWidgets/qwidget.h>
#include "ui_Calibration.h"
#include "Calibrator.h"
#include "CameraController.h"
#include <qtimer>

#define CALIBRATION_DATA_PATH_LEFT "Calibration/Left/"
#define CALIBRATION_DATA_PATH_RIGHT "Calibration/Right/"
/**
 * @class	Calibration
 *
 * @brief	A Class derived from QDialog that handles the calibration process
 */
class Calibration : public QDialog 
{
	Q_OBJECT

public:
	/**
	 * @fn	Calibration::Calibration(CameraControl::Controller* cameracontroller, int idCamL, int idCamR, QDialog *parent = Q_NULLPTR);
	 *
	 * @brief	Constructor, initialices parameters and classes
	 *
	 * @param [in,out]	cameracontroller	If non-null, the cameracontroller.
	 * @param 		  	idCamL				The identifier camera l.
	 * @param 		  	idCamR				The identifier camera r.
	 * @param [in,out]	parent				(Optional) If non-null, the parent.
	 */
	Calibration(CameraControl::Controller* cameracontroller, int idCamL, int idCamR, QDialog *parent = Q_NULLPTR);
	QTimer *timer;  ///< The timer
	StereoVision3D::Calibrator calibratorL; ///< The calibrator for the left camera
	StereoVision3D::Calibrator calibratorR; ///< The calibrator for the right camera
	StereoVision3D::StereoCalibrationData scd;  ///< The StereoCalibrationData

private slots:
	/**
	 * @fn	void Calibration::on_pushButtonCapture_clicked();
	 *
	 * @brief	Handles push button capture clicked signals
	 */
	void on_pushButtonCapture_clicked();
	/**
	 * @fn	void Calibration::timerTick();
	 *
	 * @brief	Handles timer tick signals
	 */
	void timerTick();


private:

	/**
	 * @fn	void Calibration::updateCapturedCount();
	 *
	 * @brief	Updates the captured count
	 */
	void updateCapturedCount();
	/**
	 * @fn	void Calibration::setIndicatorBoard(bool found);
	 *
	 * @brief	Sets indicator board ro green:found or red:notfound
	 *
	 * @param	found Indicator starus
	 */
	void setIndicatorBoard(bool found);
	/**
	 * @fn	void Calibration::timerInit();
	 *
	 * @brief	Timer initialize
	 */
	void timerInit();

	Ui::Calibration* ui;	///< The user interface
	CameraControl::Controller* camcontroller;   ///< The camcontroller
	int idCamL; ///< The identifier camera L
	int idCamR; ///< The identifier camera R
	cv::Mat camL;   ///< The camera L
	cv::Mat camR;   ///< The camera R
	int capturedcount = 0;  ///< The capturedcounter
};
