/**
 * @file	SerialController.h.
 *
 * @brief	Declares the serial controller class
 */
#pragma once
#include <QtSerialPort\qserialport.h>
#include <QtSerialPort\qserialportinfo.h>
#include <qvector.h>
/**
 * @namespace	SerialController
 *
 * @brief	Handles all communication over serial(bluetooth)
 */
namespace SerialController
{
	/**
	 * @class	Controller
	 *
	 * @brief	The Serial-Port communication controller
	 */
	class Controller
	{
	public:
		/**
		 * @fn	Controller::Controller();
		 *
		 * @brief	Default constructor, initialices SerialPort
		 */
		Controller();
		/**
		 * @fn	QList<QSerialPortInfo> Controller::getDevices();
		 *
		 * @brief	Gets all connected devices
		 *
		 * @returns	The devices.
		 */
		QList<QSerialPortInfo> getDevices();
		/**
		 * @fn	void Controller::connectToDevice(QString portinfo);
		 *
		 * @brief	Connects to device
		 *
		 * @param	portinfo	The portinfo.
		 */
		void connectToDevice(QString portinfo);
		/**
		 * @fn	void Controller::sendData(QString data);
		 *
		 * @brief	Sends a string over serial
		 *
		 * @param	data	The data.
		 */
		void sendData(QString data);
		/**
		 * @fn	void Controller::sendData(QByteArray data);
		 *
		 * @brief	Sends a byte over serial
		 *
		 * @param	data	The data.
		 */
		void sendData(QByteArray data);
		/**
		 * @fn	void Controller::sendData(QString name, uint16_t data);
		 *
		 * @brief	Sends an int with identifiert
		 *
		 * @param	name	The name.
		 * @param	data	The data.
		 */
		void sendData(QString name, uint16_t data);
		/**
		 * @fn	void Controller::disconnectFromDevice();
		 *
		 * @brief	Disconnects from device
		 */
		void disconnectFromDevice();
		bool connected = 0; ///< True if connected

	private:
		QSerialPort port;   ///< The port
	};
}