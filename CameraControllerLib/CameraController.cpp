#include "stdafx.h"
#include <iostream>

#include "CameraController.h"
#include "Utility.h"

namespace CameraControl
{
	Source::~Source()
	{
		videocapture.release();
	}

	CameraSource::CameraSource(int cameraid)
	{
		id = cameraid;
		videocapture = cv::VideoCapture(id);
	}
	void CameraSource::captureFrame()
	{
		videocapture >> frame;
	}
	cv::Mat CameraSource::getFrame()
	{
		return frame;
	}

	FileSource::FileSource(std::string filepath, bool loopv)
	{
		path = filepath;
		videocapture = cv::VideoCapture(filepath);
		framecount = videocapture.get(cv::CAP_PROP_FRAME_COUNT);
		loop = loopv;
	}
	void FileSource::captureFrame()
	{
		//resets framecount on video end
		if (loop && videocapture.get(cv::CAP_PROP_POS_FRAMES) >= framecount)
			videocapture.set(cv::CAP_PROP_POS_FRAMES, 0);

		videocapture >> frame;
	}
	cv::Mat FileSource::getFrame()
	{
		return frame;
	}

	Controller::Controller()
	{
		std::cout << "Created Test" << std::endl;
	}
	bool Controller::getCameraIds(std::vector<int>* camIdx)
	{
		return Utility::EnumerateCameras(*camIdx);
	}
	bool Controller::sourceExists(int id)
	{
		return sources.count(id);
	}
	void Controller::addSource(int id, int cameraid)
	{
		if (sourceExists(id))
			sources.erase(id);

		sources[id] = new CameraSource(id);
	}
	void Controller::addSource(int id, std::string filepath, bool loop)
	{
		if (sourceExists(id))
			sources.erase(id);
		sources[id] = new FileSource(filepath, loop);
	}
	void Controller::removeSource(int id)
	{
		if (sourceExists(id))
		{
			delete sources[id];
			sources.erase(id);
		}
	}
	void Controller::clearSources()
	{
		for (auto &sorce : sources)
		{
			if (sourceExists(sorce.first))
				delete sources[sorce.first];
		}
		sources.clear();
	}
	cv::Mat Controller::getFrame(int id)
	{
		return sources[id]->getFrame();
	}
	void Controller::captureFrames()
	{
		for (auto &sorce : sources)
		{
			sorce.second->captureFrame();
		}
	}
}
