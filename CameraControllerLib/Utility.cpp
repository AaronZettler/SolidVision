#include "stdafx.h"
#include "Utility.h"

using namespace std;
using namespace cv;

namespace CameraControl
{
	//From http://answers.opencv.org/question/77871/i-get-no-video-from-the-camera-that-is-connected-with-firewire/?answer=77900#post-id-77900
	/** \brief Test all grabbing drivers and fills a vector of all available cameras CAPdrivers+ID
	*
	* For each CAPdrivers+ID, opens the device. If success, push CA::P+ID in \c camIdx
	* A grabbing test is done just to inform the user.
	* \param camIdx[out] a vector of all readable cameras CAP+ID
	* \note remove some cout to use as function
	*/
	bool Utility::EnumerateCameras(std::vector<int> &camIdx)
	{
		camIdx.clear();
		struct CapDriver {
			int enumValue; string enumName; string comment;
		};

		vector<CapDriver> drivers;
		drivers.push_back({ CAP_DSHOW, "CAP_DSHOW", "DirectShow (via videoInput)" });

		std::string winName, driverName, driverComment;
		int driverEnum;
		Mat frame;
		bool found;
		std::cout << "Searching for cameras IDs..." << endl << endl;
		for (int drv = 0; drv < drivers.size(); drv++)
		{
			driverName = drivers[drv].enumName;
			driverEnum = drivers[drv].enumValue;
			driverComment = drivers[drv].comment;
			std::cout << "Testing driver " << driverName << "...";
			found = false;

			int maxID = 100; //100 IDs between drivers
			if (driverEnum == CAP_VFW)
				maxID = 10; //VWF opens same camera after 10 ?!?
			else if (driverEnum == CAP_ANDROID)
				maxID = 98; //98 and 99 are front and back cam
			//else if ((driverEnum == CAP_ANDROID_FRONT) || (driverEnum == CAP_ANDROID_BACK))
			//	maxID = 1;

			for (int idx = 0; idx <maxID; idx++)
			{
				VideoCapture cap(driverEnum + idx);  // open the camera
				if (cap.isOpened())                  // check if we succeeded
				{
					found = true;
					camIdx.push_back(idx);  // vector of all available cameras
					cap >> frame;
					if (frame.empty())
						std::cout << endl << driverName << "+" << idx << "\t opens: OK \t grabs: FAIL";
					else
						std::cout << endl << driverName << "+" << idx << "\t opens: OK \t grabs: OK";
					// display the frame
					// imshow(driverName + "+" + to_string(idx), frame); waitKey(1);
				}
				cap.release();
			}
			if (!found) cout << "Nothing !" << endl;
			cout << endl;
		}
		cout << camIdx.size() << " camera IDs has been found" << std::endl;

		return (camIdx.size()>0); // returns success
		return false;
	}
}
