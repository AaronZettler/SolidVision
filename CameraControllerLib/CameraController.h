/**
 * @file	CameraController.h.
 *
 * @brief	Declares the camera controller class
 */
#pragma once
#include <iostream>
#include<opencv2\opencv.hpp>
#include <map>

/**
 * @namespace	CameraControl
 *
 * @brief	All camera related classes
 */
namespace CameraControl
{
	/**
	 * @class	Source
	 *
	 * @brief	General video source
	 */
	class Source
	{
	public:
		/**
		* @fn	Source::~Source();
		*
		* @brief	Destructor
		* @
		*/
		~Source();

		/**
		* @fn	virtual void Source::captureFrame() = 0;
		*
		* @brief	Captures a frame
		*/
		virtual void captureFrame() = 0;
		/**
		* @fn	virtual cv::Mat Source::getFrame() = 0;
		*
		* @brief	Returns the captured frame
		*
		* @returns	The frame.
		*/
		virtual cv::Mat getFrame() = 0;
		cv::VideoCapture videocapture;  ///< The OpenCV videocapture
		cv::Mat frame;  ///< The captured frame
	};
	/**
	* @class	CameraSource
	*
	* @brief	Video source from camera
	*/
	class CameraSource : public Source
	{
	public:
		/**
		* @fn	CameraSource::CameraSource(int cameraid);
		*
		* @brief	Constructor (creates OpenCV VideoCapture from cameraid)
		*
		* @param	cameraid	The cameraid.
		*/
		CameraSource(int cameraid);

		/**
		* @fn	void CameraSource::captureFrame();
		*
		* @brief	Captures a frame (from Camera)
		*/
		void captureFrame();
		/**
		* @fn	cv::Mat CameraSource::getFrame();
		*
		* @brief	Returns the captured frame
		*
		* @returns	The frame.
		*/
		cv::Mat getFrame();

	private:
		int id; ///< The Camera ID
	};
	/**
	* @class	FileSource
	*
	* @brief	Video source from file (.avi)
	*/
	class FileSource : public Source
	{
	public:
		/**
		* @fn	FileSource::FileSource(std::string filepath, bool loop);
		*
		* @brief	Constructor (creates OpenCV VideoCapture from filepath)
		*
		* @param	filepath	The filepath.
		* @param	loop		True to loop.
		*/
		FileSource(std::string filepath, bool loop);

		/**
		* @fn	void FileSource::captureFrame();
		*
		* @brief	Captures a frame (from File)
		*/
		void captureFrame();
		/**
		* @fn	cv::Mat FileSource::getFrame();
		*
		* @brief	Returns the captured frame
		*
		* @returns	The frame.
		*/
		cv::Mat getFrame();
		bool loop = false;  ///< True to repeat when the end of the video is reached

	private:
		std::string path;   ///< Full pathname of the file
		int framecount; ///< The framecount
	};
	/**
	* @class	Controller
	*
	* @brief	Handle's camera identification and frame capturing.
	*/
	class Controller
	{
	public:
		/**
		* @fn	Controller::Controller();
		*
		* @brief	Default constructor
		*/
		Controller();

		/**
		* @fn	bool Controller::getCameraIds(std::vector<int>* camIdx);
		*
		* @brief	Gets camera identifiers by searching for connected cameras
		*
		* @param [in,out]  The result of the camera query
		*
		* @returns	True if it succeeds, false if it fails.
		*/
		bool getCameraIds(std::vector<int>* camIdx);
		/**
		* @fn	bool Controller::sourceExists(int id);
		*
		* @brief	Queries if a given source exists
		*
		* @param	id	The identifier.
		*
		* @returns	True if it succeeds, false if it fails.
		*/
		bool sourceExists(int id);
		/**
		* @fn	void Controller::addSource(int idSrc, int cameraid);
		*
		* @brief	Adds a camera source with 'cameraid' to sources map, overrides existing
		*
		* @param	idSrc   	The identifier.
		* @param	cameraid	The cameraid.
		*/
		void addSource(int idSrc, int cameraid);
		/**
		* @fn	void Controller::addSource(int id, std::string filepath, bool loop = false);
		*
		* @brief	Adds a file source with 'cameraid' to sources map, overrides existing
		*
		* @param	id			The identifier.
		* @param	filepath	The filepath.
		* @param	loop		(Optional) True to loop.
		*/
		void addSource(int id, std::string filepath, bool loop = false);
		/**
		* @fn	void Controller::removeSource(int id);
		*
		* @brief	Removes the source with the given id
		*
		* @param	id	The identifier.
		*/
		void removeSource(int id);
		/**
		* @fn	void Controller::clearSources();
		*
		* @brief	Clears the sources map
		*/
		void clearSources();
		/**
		* @fn	void Controller::captureFrames();
		*
		* @brief	Capture frames of all videosources in sources map
		*/
		void captureFrames();
		/**
		* @fn	cv::Mat Controller::getFrame(int id);
		*
		* @brief	Gets a frame
		*
		* @param	id	The identifier.
		*
		* @returns	Returns the captured frame with given id
		*/
		cv::Mat getFrame(int id);

	private:
		std::map<int, Source *> sources; ///< Map of all video sources (camera of file)
	};
}