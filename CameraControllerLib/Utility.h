#pragma once

/**
* @namespace	CameraControl
*
* @brief	All camera related Classes
*/
namespace CameraControl
{
	/**
	 * @class	Utility
	 *
	 * @brief	Class for utility functions
	 */
	class Utility
	{
	public:
		/**
		 * @fn	static bool Utility::EnumerateCameras(std::vector<int> &camIdx);
		 *
		 * @brief	Enumerates all connected camera devices
		 *
		 * @param [in,out]	camIdx	The result of the camera query
		 *
		 * @returns	True if it succeeds, false if it fails.
		 */
		static bool EnumerateCameras(std::vector<int> &camIdx);
	};
}