#include "stdafx.h"
#include "ColorTracker.h"

namespace ObjectTracking
{
	ColorMargins ColorTracker::getMargins(cv::Mat img, int marginsize)
	{
		cv::Mat hsvImg;
		cv::cvtColor(img, hsvImg, cv::COLOR_BGR2HSV);

		//get img values at position
		cv::Size size = img.size();
		int h = hsvImg.at<cv::Vec3b>(cv::Point(size.width / 2, size.height / 2))[0];
		int s = hsvImg.at<cv::Vec3b>(cv::Point(size.width / 2, size.height / 2))[1];
		int v = hsvImg.at<cv::Vec3b>(cv::Point(size.width / 2, size.height / 2))[2];

		//limit margins to max/min values
		if (h > 255 - (marginsize / 2)) h = 255 - (marginsize / 2);
		if (s > 255 - (marginsize / 2)) s = 255 - (marginsize / 2);
		if (v > 255 - (marginsize / 2)) v = 255 - (marginsize / 2);

		if (h < (marginsize / 2)) h = (marginsize / 2);
		if (s < (marginsize / 2)) s = (marginsize / 2);
		if (v < (marginsize / 2)) v = (marginsize / 2);

		//set margins
		ColorMargins margins;
		margins.lowH = h - (marginsize / 2);
		margins.highH = h + (marginsize / 2);

		margins.lowS = s - (marginsize / 2);
		margins.highS = s + (marginsize / 2);

		margins.lowV = v - (marginsize / 2);
		margins.highV = v + (marginsize / 2);

		return margins;
	}
	cv::Point ColorTracker::detectObject(cv::Mat img, ColorMargins margins)
	{
		cv::Mat hsvImg, threshImg;

		//Convert Original Image to HSV Thresh Image
		cv::cvtColor(img, hsvImg, cv::COLOR_BGR2HSV);      
		cv::inRange(hsvImg, cv::Scalar(margins.lowH, margins.lowS, margins.lowV), 
							cv::Scalar(margins.highH, margins.highS, margins.highV), threshImg);

		//Apply Blurr, Dialate, Erode Filters
		cv::GaussianBlur(threshImg, threshImg, cv::Size(3, 3), 0);   
		cv::dilate(threshImg, threshImg, 0);        
		cv::erode(threshImg, threshImg, 0);         

		//Get cv momenst
		cv::Moments m = cv::moments(threshImg, true);
		
		//Calculate position of Object
		cv::Point p(0,0);
		if (m.m00 == 0.0)
		{
			p.x = 0;
			p.y = 0;
		}
		else
		{
			p.x = m.m10 / m.m00;
			p.y = m.m01 / m.m00;
		}

		return p;
	}
	void ColorTracker::drawMarginMeasurePoint(cv::Mat img, int size)
	{
		cv::Size imsize = img.size();
		cv::circle(img, cv::Point(imsize.width / 2, imsize.height / 2), size, cv::Scalar(255, 255, 0), cv::FILLED);
	}
	void ColorTracker::drawDetectionPoint(cv::Mat img, cv::Point position, int size)
	{
		cv::circle(img, position, size/10, cv::Scalar(0, 255, 0), cv::FILLED);              // thickness							 // draw red circle around object detected 
		cv::circle(img, position, size, cv::Scalar(0, 0, 255), 3);
	}
}