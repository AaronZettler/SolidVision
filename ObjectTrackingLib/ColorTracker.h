/**
 * @file	ColorTracker.h.
 *
 * @brief	Declares the ColorTracker class and structs
 */
#pragma once
#include<opencv2/opencv.hpp>

/**
 * @namespace	ObjectTracking
 *
 * @brief	All tracking related classes
 */
namespace ObjectTracking
{
	/**
	 * @struct	ColorMargins
	 *
	 * @brief	The color margins for the img filtering.
	 */
	struct ColorMargins
	{
		int lowH = 0;   ///< Set Hue
		int highH = 0;  ///< The high

		int lowS = 0;   ///< Set Saturation
		int highS = 0;  ///< The high s

		int lowV = 0;   ///< Set Value
		int highV = 0;  ///< The high v
	};
	/**
	 * @class	ColorTracker
	 *
	 * @brief	Tracks an object by its color
	 */
	class ColorTracker
	{
	public:
		/**
		 * @fn	static ColorMargins ColorTracker::getMargins(cv::Mat img, int marginsize);
		 *
		 * @brief	Gets the margins, uses the color at img-center
		 *
		 * @param	img		  	The image.
		 * @param	marginsize	The marginsize, distance between min and max values
		 *
		 * @returns	The margins.
		 */
		static ColorMargins getMargins(cv::Mat img, int marginsize);
		/**
		 * @fn	static cv::Point ColorTracker::detectObject(cv::Mat img, ColorMargins margins);
		 *
		 * @brief	Detects the position of the colored object by calculating its center 
		 *
		 * @param	img	   	The image.
		 * @param	margins	The margins.
		 *
		 * @returns	A cv::Point.
		 */
		static cv::Point detectObject(cv::Mat img, ColorMargins margins);
		/**
		 * @fn	static void ColorTracker::drawMarginMeasurePoint(cv::Mat img, int size);
		 *
		 * @brief	Draw margin measure point (at img-center)
		 *
		 * @param	img 	The image.
		 * @param	size	The size of circle.
		 */
		static void drawMarginMeasurePoint(cv::Mat img, int size);
		/**
		 * @fn	static void ColorTracker::drawDetectionPoint(cv::Mat img, cv::Point position, int size);
		 *
		 * @brief	Draw the measured object-position
		 *
		 * @param	img			The image.
		 * @param	position	The position.
		 * @param	size		The size of circle.
		 */
		static void drawDetectionPoint(cv::Mat img, cv::Point position, int size);
	};
}