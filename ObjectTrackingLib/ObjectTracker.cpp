#include "stdafx.h"
#include "ObjectTracker.h"

namespace ObjectTracking
{
	void CascadeTracker::initTracker(std::string pathCascade)
	{
		object_cascade.load(pathCascade);
	}
	void CascadeTracker::detectObjects(cv::Mat img)
	{
		cv::Mat imggray;

		//Prepare img for detection
		cvtColor(img, imggray, cv::COLOR_BGR2GRAY);
		equalizeHist(imggray, imggray);

		//Detect Objects
		object_cascade.detectMultiScale(imggray, objects, 1.1, 2, 0 | cv::CASCADE_SCALE_IMAGE, cv::Size(60, 60));
		if (objects.size() > 0)
			position = cv::Point(objects[0].x + objects[0].width / 4, objects[0].y + objects[0].height / 2);
	}
	void CascadeTracker::drawDetection(cv::Mat img)
	{
		for (int i = 0; i < objects.size(); i++)
		{
			cv::Point center(objects[i].x + objects[i].width / 2, objects[i].y + objects[i].height / 2);
			cv::ellipse(img, center, cv::Size(objects[i].width / 2, objects[i].height / 2), 0, 0, 360, cv::Scalar(255, 0, 0), 4, 8, 0);
		}
	}
	void CascadeTracker::showDetection(cv::Mat img)
	{
		cv::imshow("ObjectTracker", img);
	}
}