/**
 * @file	ObjectTracker.h.
 *
 * @brief	Declares the CascadeTracker class
 */
#pragma once
#include<opencv2/opencv.hpp>
/**
 * @namespace	ObjectTracking
 *
 * @brief	All tracking related classes.
 */
namespace ObjectTracking
{
	/**
	 * @class	CascadeTracker
	 *
	 * @brief	A cascade tracker.
	 */
	class CascadeTracker
	{
	public:
		/**
		 * @fn	void CascadeTracker::initTracker(std::string pathCascade);
		 *
		 * @brief	Initializes the tracker
		 *
		 * @param	pathCascade	The path to the cascade.xml file for the cascade-tracker classifier 
		 */
		void initTracker(std::string pathCascade);
		/**
		 * @fn	void CascadeTracker::detectObjects(cv::Mat img);
		 *
		 * @brief	Detects objects in img
		 *
		 * @param	img	The image.
		 */
		void detectObjects(cv::Mat img);
		/**
		 * @fn	void CascadeTracker::drawDetection(cv::Mat img);
		 *
		 * @brief	Draws the detected objects to img  
		 *
		 * @param	img	The image.
		 */
		void drawDetection(cv::Mat img);
		/**
		 * @fn	void CascadeTracker::showDetection(cv::Mat img);
		 *
		 * @brief	Shows the detection (with cv::imshow)
		 *
		 * @param	img	The image.
		 */
		void showDetection(cv::Mat img);
		cv::Point position; ///< The position of the first detected object

	private:
		std::vector<cv::Rect> objects;  ///< The detected objects
		cv::CascadeClassifier object_cascade;   ///< The cascade classifier 
	};
}