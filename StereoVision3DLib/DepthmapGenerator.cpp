#include "stdafx.h"
#include "DepthmapGenerator.h"
#include "opencv2/ximgproc.hpp"

namespace StereoVision3D
{
	DepthmapGenerator::DepthmapGenerator()
	{
	stereoBM = cv::StereoBM::create(0, 21);
	}
	DepthmapGenerator::~DepthmapGenerator()
	{
		writer.release();
	}

	void DepthmapGenerator::generate(cv::Mat webcamL, cv::Mat webcamR, int blurfactor)
	{
		if (newdispmode)
		{
			int max_disp = 160;
			cv::Mat left_for_matcher, right_for_matcher, left_disp, right_disp, filtered_disp;
			int wsize = 7;
			double lambda = 8000.0, sigma = 1.5;

			max_disp /= 2;
			if (max_disp % 16 != 0)
				max_disp += 16 - (max_disp % 16);
			cv::resize(webcamL, left_for_matcher, cv::Size(), 0.5, 0.5);
			cv::resize(webcamR, right_for_matcher, cv::Size(), 0.5, 0.5);

			/*left_for_matcher = webcamL;
			right_for_matcher = webcamR;*/

			cv::Ptr<cv::StereoBM> left_matcher = cv::StereoBM::create(max_disp, wsize);
			cv::Ptr<cv::ximgproc::DisparityWLSFilter> wls_filter = cv::ximgproc::createDisparityWLSFilter(left_matcher);
			cv::Ptr<cv::StereoMatcher> right_matcher = cv::ximgproc::createRightMatcher(left_matcher);
			cv::cvtColor(left_for_matcher, left_for_matcher, cv::COLOR_BGR2GRAY);
			cv::cvtColor(right_for_matcher, right_for_matcher, cv::COLOR_BGR2GRAY);
			left_matcher->compute(left_for_matcher, right_for_matcher, left_disp);
			right_matcher->compute(right_for_matcher, left_for_matcher, right_disp);

			wls_filter->setLambda(lambda);
			wls_filter->setSigmaColor(sigma);

			wls_filter->filter(left_disp, webcamL, filtered_disp, right_disp);

			double vis_mult = 5.0;
			//cv::ximgproc::getDisparityVis(filtered_disp, depthMap, vis_mult);
			depthMap = filtered_disp;
			//std::string maptype = type2str(filtered_disp.type());//16SC1
		}
		else
		{
			//OLD____________________________________________________________________________________________-
			cv::Mat grayL, grayR;

			//get Image Data
			cv::Size imgsize = webcamL.size();
			depthMap = cv::Mat(imgsize.height, imgsize.width, CV_16S);

			//convert to gry
			cv::cvtColor(webcamL, grayL, cv::COLOR_BGR2GRAY);
			cv::cvtColor(webcamR, grayR, cv::COLOR_BGR2GRAY);

			//add Blur if needed
			for (int i = 0; i < blurfactor; i++)
			{
				cv::GaussianBlur(grayL, grayL, cv::Size(3, 3), 0);
				cv::GaussianBlur(grayR, grayR, cv::Size(3, 3), 0);
			}

			//compute Stereo Map
			stereoBM->compute(grayL, grayR, depthMap);
			std::string maptype = type2str(depthMap.type());
		}
	}
	void DepthmapGenerator::setFilter(int erode, int dialate, FilterMode filtermode)
	{
		filterMode = filtermode;
		erosionMat = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2 * erode + 1, 2 * erode + 1), cv::Point(erode, erode));
		dialationMat = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2 * dialate + 1, 2 * dialate + 1), cv::Point(dialate, dialate));
	}
	void DepthmapGenerator::filterMap(cv::Mat depthmapnormalized)
	{
		switch (filterMode)
		{
		case FilterMode::BASIC:
			applyBasicFilter(depthmapnormalized);
			break;
		case FilterMode::DETAILED:
			applyDetailedFilter(depthmapnormalized);
			break;
		default:
			break;
		}
	}
	cv::Mat DepthmapGenerator::getDepthMap()
	{
		return depthMap;
	}
	cv::Mat DepthmapGenerator::getDepthMapNormalized()
	{
		cv::normalize(depthMap, normalized, 0, 255, cv::NORM_MINMAX, CV_8U);
		return normalized;
	}
	cv::Mat DepthmapGenerator::getDepthMapColored(cv::Mat img, cv::Mat depthmapnormalized)
	{
		cv::Mat colored;
		cv::applyColorMap(depthmapnormalized, colored, cv::COLORMAP_JET);
		cv::addWeighted(colored, 0.5, img, 0.5, 0.0, colored);
		return colored;
	}
	cv::Vec3f DepthmapGenerator::getMeasurementAtPosition(int row, int col, cv::Mat Q)
	{
		cv::Mat disparity32F;
		std::string typetest = type2str(depthMap.type());
		depthMap.clone().convertTo(disparity32F, CV_32F, 1. / 16);

		cv::Vec3f XYZ(1,1,1);
		cv::Mat_<double> vec_tmp(4, 1);
		vec_tmp(0) = row; vec_tmp(1) = col; vec_tmp(2) = disparity32F.at<float>(row, col); vec_tmp(3) = 1;
		vec_tmp = Q*vec_tmp;
		vec_tmp /= vec_tmp(3);

		XYZ[0] = vec_tmp(0);
		XYZ[1] = vec_tmp(1);
		XYZ[2] = vec_tmp(2);

		return XYZ;
		//return depthMap.at<cv::int16_t>(col,row);
	}
	void DepthmapGenerator::createtWindow()
	{
		cv::namedWindow("DepthMap", cv::WINDOW_NORMAL);
	}
	void DepthmapGenerator::destroyWindow()
	{
		cv::destroyWindow("DepthMap");
	}
	void DepthmapGenerator::show(cv::Mat depthMap)
	{
		cv::imshow("DepthMap", depthMap);
	}
	void DepthmapGenerator::initWriter(cv::Size framesize)
	{
		writer.open("DepthMap.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 10, framesize, true);

		if (!writer.isOpened())
			std::cout << "Could not open VideoWriter at (DepthmapGenerator)" << std::endl;
	}

	void DepthmapGenerator::drawCrosshairs(cv::Mat img, int posrow, int poscol, int size, int thickness)
	{
		int rows = img.rows;
		int cols = img.cols;

		cv::line(img, cv::Point(posrow - size / 2, poscol), cv::Point(posrow + size / 2, poscol), cv::Scalar(0, 0, 255), thickness);
		cv::line(img, cv::Point(posrow, poscol - size / 2), cv::Point(posrow, poscol + size / 2), cv::Scalar(0, 0, 255), thickness);
	}

	void DepthmapGenerator::applyBasicFilter(cv::Mat depthmapnormalized)
	{
		cv::erode(depthmapnormalized, depthmapnormalized, erosionMat);
		cv::dilate(depthmapnormalized, depthmapnormalized, dialationMat);
	}
	void DepthmapGenerator::applyDetailedFilter(cv::Mat depthmapnormalized)
	{
		cv::Mat mask;

		cv::erode(depthmapnormalized, mask, erosionMat);
		cv::dilate(mask, mask, dialationMat);

		cv::bitwise_and(depthmapnormalized, mask, depthmapnormalized);
	}
	void DepthmapGenerator::save(cv::Mat img)
	{
		writer.write(img);
	}
}