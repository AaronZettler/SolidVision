/**
 * @file	DepthmapGenerator.h.
 *
 * @brief	Declares the depthmap generator class
 */
#pragma once
#include<iostream>
#include<opencv2/opencv.hpp>

 /**
 * @namespace	StereoVision3D
 *
 * @brief	All StereoVision related classes.
 */
namespace StereoVision3D
{
	/**
	 * @enum	Mode
	 *
	 * @brief	The display mode for the visual depth-map
	 */
	enum Mode { NORMAL, COLOR };
	/**
	 * @enum	The filter mode for the visual depth-map
	 *
	 * @brief	Values that represent filter modes
	 */
	enum FilterMode { BASIC, DETAILED };
	/**
	 * @class	DepthmapGenerator
	 *
	 * @brief	Class that utilizes 2 img inputs to generate a depth-map and uses
	 * 			calibration data to get a 3D-Measurement in "real" units 
	 */
	class DepthmapGenerator
	{
	public:

		/**
		 * @fn	DepthmapGenerator::DepthmapGenerator();
		 *
		 * @brief	Default constructor
		 */
		DepthmapGenerator();
		/**
		 * @fn	DepthmapGenerator::~DepthmapGenerator();
		 *
		 * @brief	Destructor
		 */
		~DepthmapGenerator();
		/**
		 * @fn	void DepthmapGenerator::generate(cv::Mat webcamL, cv::Mat webcamR, int blurfactor = 0);
		 *
		 * @brief	Generates either a simple depth map or a filterd version depending on newdispmode
		 *
		 * @param	webcamL   	The webcam l.
		 * @param	webcamR   	The webcam r.
		 * @param	blurfactor	(Optional) The blurfactor.
		 */
		void generate(cv::Mat webcamL, cv::Mat webcamR, int blurfactor = 0);
		/**
		 * @fn	void DepthmapGenerator::setFilter(int erode, int dialate, FilterMode filtermode);
		 *
		 * @brief	Sets the filter used for FIlterMode
		 *
		 * @param	erode	  	The erode.
		 * @param	dialate   	The dialate.
		 * @param	filtermode	The filtermode.
		 */
		void setFilter(int erode, int dialate, FilterMode filtermode);
		/**
		 * @fn	void DepthmapGenerator::filterMap(cv::Mat depthmapnormalized);
		 *
		 * @brief	Applies filters to the visual depth-map
		 *
		 * @param	depthmapnormalized	The depthmapnormalized.
		 */
		void filterMap(cv::Mat depthmapnormalized);
		/**
		 * @fn	cv::Mat DepthmapGenerator::getDepthMap();
		 *
		 * @brief	Gets depth map
		 *
		 * @returns	The depth map.
		 */
		cv::Mat getDepthMap();
		/**
		 * @fn	cv::Mat DepthmapGenerator::getDepthMapNormalized();
		 *
		 * @brief	Gets the normalized depth map 
		 *
		 * @returns	The depth map normalized.
		 */
		cv::Mat getDepthMapNormalized();
		/**
		 * @fn	cv::Mat DepthmapGenerator::getDepthMapColored(cv::Mat img, cv::Mat depthmapnormalized);
		 *
		 * @brief	Gets a representation of the depth-map in colors
		 *
		 * @param	img				  	The image.
		 * @param	depthmapnormalized	The depthmapnormalized.
		 *
		 * @returns	The depth map colored.
		 */
		cv::Mat getDepthMapColored(cv::Mat img, cv::Mat depthmapnormalized);
		/**
		 * @fn	cv::Vec3f DepthmapGenerator::getMeasurementAtPosition(int row, int col, cv::Mat Q);
		 *
		 * @brief	Gets measurement at position
		 *
		 * @param	row	The row.
		 * @param	col	The col.
		 * @param	Q  	A cv::Mat to process.
		 *
		 * @returns	The measurement at position.
		 */
		cv::Vec3f getMeasurementAtPosition(int row, int col, cv::Mat Q);
		/**
		 * @fn	void DepthmapGenerator::createtWindow();
		 *
		 * @brief	Createts a cv window to display the depth-map
		 */
		void createtWindow();
		/**
		 * @fn	void DepthmapGenerator::destroyWindow();
		 *
		 * @brief	Destroys the depth-map display window
		 */
		void destroyWindow();
		/**
		 * @fn	void DepthmapGenerator::show(cv::Mat depthMap);
		 *
		 * @brief	Shows the depth map in Window
		 *
		 * @param	depthMap	The depth map.
		 */
		void show(cv::Mat depthMap);
		/**
		 * @fn	void DepthmapGenerator::initWriter(cv::Size framesize);
		 *
		 * @brief	Initializes the cv VideoWriter
		 *
		 * @param	framesize	The framesize.
		 */
		void initWriter(cv::Size framesize);
		
		/**
		 * @fn	void DepthmapGenerator::drawCrosshairs(cv::Mat img, int posrow, int poscol, int size, int thickness);
		 *
		 * @brief	Draws Crosshairs at position
		 *
		 * @param	img		 	The image.
		 * @param	posrow   	The posrow.
		 * @param	poscol   	The poscol.
		 * @param	size	 	The size.
		 * @param	thickness	The thickness.
		 */
		void drawCrosshairs(cv::Mat img, int posrow, int poscol, int size, int thickness);

		Mode mode = Mode::NORMAL;   ///< The display mode (Grayscale or Color)
		bool newdispmode = false;   ///< Enables the new depth-map filter method

	private:

		/**
		 * @fn	void DepthmapGenerator::applyBasicFilter(cv::Mat depthmapnormalized);
		 *
		 * @brief	Applies a basic filter to depthmapnormalized
		 *
		 * @param	depthmapnormalized	The depthmapnormalized.
		 */
		void applyBasicFilter(cv::Mat depthmapnormalized);
		/**
		 * @fn	void DepthmapGenerator::applyDetailedFilter(cv::Mat depthmapnormalized);
		 *
		 * @brief	Applies a detailed to depthmapnormalized
		 *
		 * @param	depthmapnormalized	The depthmapnormalized.
		 */
		void applyDetailedFilter(cv::Mat depthmapnormalized);
		/**
		 * @fn	void DepthmapGenerator::save(cv::Mat img);
		 *
		 * @brief	Saves the given image
		 *
		 * @param	img	The Image to save.
		 */
		void save(cv::Mat img);

		//Stereo
		cv::Ptr<cv::StereoBM> stereoBM; ///< The stereo bm
		cv::Mat depthMap;   ///< The depth map (cv Type:16SC1)
		cv::Mat normalized; ///< The normalized map
		cv::VideoWriter writer; ///< The video writer

		//Filter
		FilterMode filterMode;  ///< The filter mode
		cv::Mat filterMask; ///< The filter mask
		int erosion_size = 0;   ///< Size of the erosion
		int dilate_size = 0;	///< Size of the dilate
		cv::Mat erosionMat; ///< The erosion matrix
		cv::Mat dialationMat;   ///< The dialation matrix
	};
}