#include "stdafx.h"
#include "Calibrator.h"
#include <iostream>
#include <fstream>
#define CHESSBOARD_SQUARE_SIZE 25 //mm
using namespace std;

namespace StereoVision3D
{
	Calibrator::Calibrator(int measurements, cv::Size chessboard)
	{
		numBoards = measurements;
		numCornersHor = chessboard.width;
		numCornersVert = chessboard.height;
		numSquares = numCornersHor * numCornersVert;
		board_sz = cv::Size(numCornersHor, numCornersVert);

		//create object points
		for (int j = 0; j < numSquares; j++)
			obj.push_back(cv::Point3f((j / numCornersHor)*CHESSBOARD_SQUARE_SIZE, (j%numCornersHor)*CHESSBOARD_SQUARE_SIZE, 0.0f));
	}
	void Calibrator::init()
	{
		image_points.clear();
		object_points.clear();
		count = 0;
	}

	void Calibrator::detectBoard(cv::Mat img_in)
	{
		img = img_in;
		imgcorners = img.clone();
		cv::Mat gray;
		cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);

		found = findChessboardCorners(gray, board_sz, corners, 0);
		if (found)
		{
			cornerSubPix(gray, corners, cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::EPS | cv::TermCriteria::MAX_ITER, 30, 0.1));
			drawChessboardCorners(imgcorners, board_sz, corners, found);
		}
	}
	void Calibrator::useBoard()
	{
		image_points.push_back(corners);
		object_points.push_back(obj);
	}
	void Calibrator::saveBoard(std::string path)
	{
		std::string paht1 = getIndexedFilePaht(path, count);
		std::string path2 = getIndexedFilePaht(path, count, "_Corners");
		cv::imwrite(paht1, img);
		cv::imwrite(path2, imgcorners);
		std::cout << "Saved IMG " << count << std::endl;
		count++;
	}

	void Calibrator::calibrateImg(int aspectratio)
	{
		calibrationdata.intrinsic.ptr<float>(0)[0] = aspectratio;
		calibrationdata.intrinsic.ptr<float>(1)[1] = aspectratio;

		CalibrationData *dat = &calibrationdata;

		cv::calibrateCamera(object_points, image_points, img.size(), dat->intrinsic, dat->distCoeffs, dat->rvecs, dat->tvecs);
		ready = true;
	}
	StereoCalibrationData Calibrator::calibrateStereoImg(Calibrator left, Calibrator right)
	{
		StereoCalibrationData scd;

		//Convert objectpoints to right format
		std::vector<std::vector<cv::Point3f>> object_points;
		std::copy(left.object_points.begin(), left.object_points.end(), std::back_inserter(object_points));

		//Combine image points
		std::vector< std::vector< cv::Point2f > > left_img_points, right_img_points;
		for (int i = 0; i < left.image_points.size(); i++) {
			std::vector< cv::Point2f > v1, v2;
			for (int j = 0; j < left.image_points[i].size(); j++) {
				v1.push_back(cv::Point2f((double)left.image_points[i][j].x, (double)left.image_points[i][j].y));
				v2.push_back(cv::Point2f((double)right.image_points[i][j].x, (double)right.image_points[i][j].y));
			}
			left_img_points.push_back(v1);
			right_img_points.push_back(v2);
		}

		//Input already calculated parameters
		scd.K1 = left.calibrationdata.intrinsic.clone();
		scd.K2 = right.calibrationdata.intrinsic.clone();
		scd.D1 = left.calibrationdata.distCoeffs.clone();
		scd.D2 = right.calibrationdata.distCoeffs.clone();

		try
		{
			//Calculate calibration
			cv::Size size = left.imgcorners.size();
			cv::stereoCalibrate(object_points, left_img_points, right_img_points, scd.K1, scd.D1, scd.K2, scd.D2, size, scd.R, scd.T, scd.E, scd.F, cv::CALIB_FIX_INTRINSIC);
			cv::Ptr<cv::Formatted> fmt = cv::format(scd.K1, cv::Formatter::FMT_NUMPY);

			//Output calibration results (in Debug Mode)
			DBOUT_START_BLOCK("StereoCalibrate");
			DBOUT_MAT("K1", scd.K1); DBOUT_MAT("K2", scd.K2); DBOUT_MAT("D1", scd.D1);
			DBOUT_MAT("D2", scd.D2); DBOUT_MAT("R", scd.R); DBOUT_MAT("T", scd.T);
			DBOUT_MAT("E", scd.E); DBOUT_MAT("F", scd.F);
			DBOUT_END_BLOCK;

			cv::stereoRectify(scd.K1, scd.D1, scd.K2, scd.D2, size, scd.R, scd.T, scd.R1, scd.R2, scd.P1, scd.P2, scd.Q);

			//Output rectification resutls (in Debug Mode)
			DBOUT_START_BLOCK("StereoCalibrate");
			DBOUT_MAT("R1", scd.R1); DBOUT_MAT("R2", scd.R2); DBOUT_MAT("P1", scd.P1);
			DBOUT_MAT("P2", scd.P2); DBOUT_MAT("Q", scd.Q);
			DBOUT_END_BLOCK;

			//Calculate error
			scd.reprojectionerror.errorleft = computeError(left.object_points, left.image_points, left.calibrationdata.rvecs, left.calibrationdata.tvecs, left.calibrationdata.intrinsic, left.calibrationdata.distCoeffs);
			scd.reprojectionerror.errorright = computeError(right.object_points, right.image_points, right.calibrationdata.rvecs, right.calibrationdata.tvecs, right.calibrationdata.intrinsic, right.calibrationdata.distCoeffs);
			scd.reprojectionerror.meanerror = (scd.reprojectionerror.errorleft + scd.reprojectionerror.errorright) / 2;
		}
		catch (cv::Exception & e)
		{
			std::string exep = e.msg;
			ofstream myfile;
			myfile.open("output.txt");
			myfile << exep;
			myfile.close();
		}

		return scd;
	}
	void Calibrator::saveCalibration(std::string path, std::string key, bool append)
	{
		cv::FileStorage filestorage;

		int flag = cv::FileStorage::WRITE;
		if (append) flag = cv::FileStorage::APPEND;

		//Save Calibration to path+name+key
		filestorage.open(path, flag);
		filestorage << "Intrinsic" + key << calibrationdata.intrinsic;
		filestorage << "DistCoeffs" + key << calibrationdata.distCoeffs;
		filestorage.release();
	}
	void Calibrator::saveStereoCalibration(StereoCalibrationData scd, std::string path, std::string key, bool append)
	{
		cv::FileStorage filestorage;

		int flag = cv::FileStorage::WRITE;
		if (append) flag = cv::FileStorage::APPEND;

		//Save Calibration to path+name+key
		filestorage.open(path, flag);
		filestorage << "K1" + key << scd.K1;
		filestorage << "K2" + key << scd.K2;
		filestorage << "R" + key << scd.R;
		filestorage << "F" + key << scd.F;
		filestorage << "E" + key << scd.E;
		filestorage << "T" + key << scd.T;
		filestorage << "D1" + key << scd.D1;
		filestorage << "D2" + key << scd.D2;

		filestorage << "R1" + key << scd.R1;
		filestorage << "R2" + key << scd.R2;
		filestorage << "P1" + key << scd.P1;
		filestorage << "P2" + key << scd.P2;
		filestorage << "Q" + key << scd.Q;

		filestorage.release();
	}
	std::string Calibrator::getIndexedFilePaht(std::string path, int index, std::string type)
	{
		return path + "Img_" + std::to_string(index) + type + ".png";
	}

	void Calibrator::loadCalibration(std::string path, std::string key)
	{
		cv::FileStorage filestorage;
		filestorage.open(path, cv::FileStorage::READ);

		//Load Calibration from path+name+key 
		//TODO Add the path option to all save/load functions
		filestorage["Intrinsic" + key] >> calibrationdata.intrinsic;
		filestorage["DistCoeffs" + key] >> calibrationdata.distCoeffs;
		filestorage.release();
		ready = true;
	}
	StereoCalibrationData Calibrator::loadStereoCalibration(std::string path, std::string key)
	{
		StereoCalibrationData scd;
		cv::FileStorage filestorage;

		//Load Calibration from path+name+key
		filestorage.open(path, cv::FileStorage::READ);
		filestorage["K1" + key] >> scd.K1;
		filestorage["K2" + key] >> scd.K2;
		filestorage["R" + key] >> scd.R;
		filestorage["F" + key] >> scd.F;
		filestorage["E" + key] >> scd.E;
		filestorage["T" + key] >> scd.T;
		filestorage["D1" + key] >> scd.D1;
		filestorage["D2" + key] >> scd.D2;

		filestorage["R1" + key] >> scd.R1;
		filestorage["R2" + key] >> scd.R2;
		filestorage["P1" + key] >> scd.P1;
		filestorage["P2" + key] >> scd.P2;
		filestorage["Q" + key] >> scd.Q;

		filestorage.release();
		return scd;
	}
	cv::Mat Calibrator::Undistort(cv::Mat img)
	{
		cv::Mat result;
		cv::undistort(img, result, calibrationdata.intrinsic, calibrationdata.distCoeffs);
		return result;
	}
	StereoMat Calibrator::Undistort(cv::Mat imgL, cv::Mat imgR, StereoCalibrationData scd)
	{
		StereoMat result;
		cv::undistort(imgL, result.imgL, scd.K1, scd.D1);
		cv::undistort(imgR, result.imgR, scd.K2, scd.D2);
		return result;
	}
	
	cv::Mat Calibrator::getImgCorners()
	{
		return imgcorners;
	}
	void Calibrator::show()
	{
		cv::namedWindow("Board Detection", cv::WINDOW_NORMAL);
		cv::imshow("Board Detection", imgcorners);
	}
	
	double Calibrator::computeError(std::vector<std::vector<cv::Point3f>> object_points, std::vector<std::vector<cv::Point2f>> image_points, std::vector<cv::Mat> rvecs, std::vector<cv::Mat> tvecs, cv::Mat intrinsic, cv::Mat distCoeffs)
	{
		std::vector<cv::Point2f> image_points2;
		double totalerror = 0;

		for (int i = 0; i < object_points.size(); i++)
		{
			cv::projectPoints(object_points[i], rvecs[i], tvecs[i], intrinsic, distCoeffs, image_points);
			double error = cv::norm(cv::Mat(image_points[i]), cv::Mat(image_points2), cv::NORM_L2) / image_points2.size();
			totalerror += error;
		}

		return totalerror / object_points.size();
	}
}
