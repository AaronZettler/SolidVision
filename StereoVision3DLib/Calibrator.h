/**
 * @file	Calibrator.h.
 *
 * @brief	Declares the StereoVision-related classes
 */
#pragma once
#include<iostream>
#include<opencv2/opencv.hpp>
#include<opencv2/calib3d.hpp>

/**
 * @namespace	StereoVision3D
 *
 * @brief	All StereoVision related classes.
 */
namespace StereoVision3D
{
	/**
	 * @struct	ReprojectionError
	 *
	 * @brief	The reprojection error produced during calibration
	 */
	struct ReprojectionError
	{
		double errorleft = 0;   ///< The error of the left camera
		double errorright = 0;	///< The error of the right camera
		double meanerror = 0;	///< The mean error
	};
	/**
	 * @struct	CalibrationData
	 *
	 * @brief	Calibration parameters for one camera
	 */
	struct CalibrationData
	{
		cv::Mat intrinsic = cv::Mat(3, 3, CV_32FC1);	///< The intrinsic parameters
		cv::Mat distCoeffs; ///< Camera distortion parameters
		std::vector<cv::Mat> rvecs; ///< Rotation vector together with the corresponding k-th translation vector
		std::vector<cv::Mat> tvecs; ///< Output vector of translation vectors estimated for each pattern view.
	};
	/**
	 * @struct	StereoMat
	 *
	 * @brief	Contains 2 cv matrices for the left and right camera
	 */
	struct StereoMat
	{
		cv::Mat imgL, imgR; ///< The 2 cameras
	};
	/**
	 * @struct	StereoCalibrationData
	 *
	 * @brief	Calibration parameters for 2 cameras
	 */
	struct StereoCalibrationData
	{
		cv::Mat K1, K2, R, F, E;	///< K1 � First camera matrix, K2 � Second camera matrix, R1 � Output 3x3 rectification transform 
		cv::Vec3d T;				///< Output translation vector between the coordinate systems of the cameras.
		cv::Mat D1, D2;				///< Input/output vector of distortion coefficients

		cv::Mat R1, R2, P1, P2, Q;	///< R � Output rotation matrix between the 1st and the 2nd camera coordinate systems, P1 � Output 3x4 projection matrix in the new (rectified) coordinate systems for the first camera.

		ReprojectionError reprojectionerror;	///< The reprojection error produced during stereo-calibration
	};

	/**
	 * @class	Calibrator
	 *
	 * @brief	This class calculates the properties of the camera and uses them to produce a "true" image.
	 * 			In addition the Q-Matrix(result of calibration) is used to 
	 * 			calculate measurements (convert the arbitrary units into a known unit like mm)
	 */
	class Calibrator
	{
	public:

		/**
		 * @fn	Calibrator::Calibrator(int measurements, cv::Size chessboard);
		 *
		 * @brief	Constructor, sets parameters for measurement
		 *
		 * @param	measurements	The ammount of measurements (amount of stereo-chessboard-pictures)
		 * @param	chessboard  	The dimensions of the chessboard (points where 2 equal colors meet)
		 */
		Calibrator(int measurements, cv::Size chessboard);
		/**
		* @fn	Calibrator::Calibrator();
		*
		* @brief	Constructor, sets parameters for measurement
		*/
		Calibrator() { Calibrator(10, cv::Size(9, 6)); } //TODO MAKE BETTER
		/**
		 * @fn	void Calibrator::init();
		 *
		 * @brief	Initializes the Calibrator
		 */
		void init();
		/**
		 * @fn	void Calibrator::detectBoard(cv::Mat img);
		 *
		 * @brief	Detects board and extracts corners
		 *
		 * @param	img	The image.
		 */
		void detectBoard(cv::Mat img);
		/**
		 * @fn	void Calibrator::useBoard();
		 *
		 * @brief	Adds board-data to collection
		 */
		void useBoard();
		/**
		 * @fn	void Calibrator::saveBoard(std::string path);
		 *
		 * @brief	Saves board and board with corners to path
		 *
		 * @param	path	Full pathname of the file.
		 */
		void saveBoard(std::string path);

		/**
		 * @fn	void Calibrator::calibrateImg(int aspectratio);
		 *
		 * @brief	Creates camera data with collected corners for one camera
		 *
		 * @param	aspectratio	The aspectratio.
		 */
		void calibrateImg(int aspectratio);
		/**
		 * @fn	static StereoCalibrationData Calibrator::calibrateStereoImg(Calibrator left, Calibrator right);
		 *
		 * @brief	Creates camera data with collected corners for 2 cameras, uses single calibration data from both calibrators
		 *
		 * @param	left 	The left.
		 * @param	right	The right.
		 *
		 * @returns	A StereoCalibrationData.
		 */
		static StereoCalibrationData calibrateStereoImg(Calibrator left, Calibrator right);
		/**
		 * @fn	void Calibrator::saveCalibration(std::string path, std::string key, bool append);
		 *
		 * @brief	Saves the calibration for one camera to a file
		 *
		 * @param	path  	Full pathname of the file.
		 * @param	key   	The key.
		 * @param	append	True to append.
		 */
		void saveCalibration(std::string path, std::string key, bool append);
		/**
		 * @fn	static void Calibrator::saveStereoCalibration(StereoCalibrationData scd, std::string path, std::string key, bool append);
		 *
		 * @brief	Saves a stereo calibration (for both cameras) to a file
		 *
		 * @param	scd   	The scd.
		 * @param	path  	Full pathname of the file.
		 * @param	key   	The key.
		 * @param	append	True to append.
		 */
		static void saveStereoCalibration(StereoCalibrationData scd, std::string path, std::string key, bool append);
		/**
		 * @fn	std::string Calibrator::getIndexedFilePaht(std::string path, int index, std::string type = "");
		 *
		 * @brief	Gets indexed file paht
		 *
		 * @param	path 	Full pathname of the file.
		 * @param	index	Zero-based index of the.
		 * @param	type 	(Optional) The type.
		 *
		 * @returns	The indexed file paht.
		 */
		std::string getIndexedFilePaht(std::string path, int index, std::string type = "");
		
		/**
		 * @fn	void Calibrator::loadCalibration(std::string path, std::string key);
		 *
		 * @brief	Loads the calibration for a single camera from a file
		 *
		 * @param	path	Full pathname of the file.
		 * @param	key 	The key.
		 */
		void loadCalibration(std::string path, std::string key);
		/**
		 * @fn	static StereoCalibrationData Calibrator::loadStereoCalibration(std::string path, std::string key);
		 *
		 * @brief	Loads the stereo calibration (for both cameras) from a file
		 *
		 * @param	path	Full pathname of the file.
		 * @param	key 	The key.
		 *
		 * @returns	The stereo calibration.
		 */
		static StereoCalibrationData loadStereoCalibration(std::string path, std::string key);
		/**
		 * @fn	cv::Mat Calibrator::Undistort(cv::Mat img);
		 *
		 * @brief	Undistorts the given image (calculates a "true" image) for one camera
		 *
		 * @param	img	The image.
		 *
		 * @returns	A cv::Mat.
		 */
		cv::Mat Undistort(cv::Mat img);
		/**
		 * @fn	static StereoMat Calibrator::Undistort(cv::Mat imgL, cv::Mat imgR, StereoCalibrationData scd);
		 *
		 * @brief	Undistorts the given sterep image (img left and img right) for one both cameras, both cameras have the same coordinate system.
		 * 			
		 *
		 * @param	imgL	The image l.
		 * @param	imgR	The image r.
		 * @param	scd 	The StereoCalibrationData.
		 *
		 * @returns	A StereoMat.
		 */
		static StereoMat Undistort(cv::Mat imgL, cv::Mat imgR, StereoCalibrationData scd);
		
		/**
		 * @fn	cv::Mat Calibrator::getImgCorners();
		 *
		 * @brief	Gets image corners
		 *
		 * @returns	The image corners.
		 */
		cv::Mat getImgCorners();

		/**
		 * @fn	void Calibrator::show();
		 *
		 * @brief	Shows the Calibration-Process in OpenCV-Window
		 */
		void show();

		int numBoards;  ///< Number of boards
		bool found; ///< If a chessboard has been found
		int count;  ///< Number of successful chessboard captures
		bool ready = false; ///< If data collection is complete
		CalibrationData calibrationdata;	///< The calibrationdata
		std::vector<std::vector<cv::Point3f>> object_points;	///< The object points
		std::vector<std::vector<cv::Point2f>> image_points; ///< The image points

	private:

		/**
		 * @fn	static double Calibrator::computeError(std::vector<std::vector<cv::Point3f>> object_points, std::vector<std::vector<cv::Point2f>> image_points, std::vector<cv::Mat> rvecs, std::vector<cv::Mat> tvecs, cv::Mat intrinsic, cv::Mat distCoeffs);
		 *
		 * @brief	Calculates the ReprojectionError after calibration
		 *
		 * @param	object_points	The object points.
		 * @param	image_points 	The image points.
		 * @param	rvecs		 	The rvecs.
		 * @param	tvecs		 	The tvecs.
		 * @param	intrinsic	 	The intrinsic.
		 * @param	distCoeffs   	The distance coeffs.
		 *
		 * @returns	The calculated error.
		 */
		static double computeError(std::vector<std::vector<cv::Point3f>> object_points, std::vector<std::vector<cv::Point2f>> image_points, std::vector<cv::Mat> rvecs, std::vector<cv::Mat> tvecs, cv::Mat intrinsic, cv::Mat distCoeffs);
		int numCornersHor;  ///< Number of corners horizontal
		int numCornersVert; ///< Number of corners vertical
		int numSquares; ///< Number of squares
		cv::Mat img;	///< The image
		cv::Mat imgcorners; ///< The image with corner detection
		cv::Size board_sz;  ///< Size of the board
		std::vector<cv::Point3f> obj;   ///< The objects
		std::vector<cv::Point2f> corners;   ///< The corners
	};
}