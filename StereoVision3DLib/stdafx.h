// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <Windows.h>
#include <iostream>
#include <sstream>

extern std::wstring StringToWString(const std::string &s);

#ifdef _DEBUG
#define DBOUT( s )            \
{                             \
	std::ostringstream os_;    \
	os_ << std::fixed << std::setprecision(2) << s;    \
	std::string str(os_.str().c_str()); \
	std::wstring wsTmp(str.begin(),str.end()); \
	OutputDebugStringW(wsTmp.c_str());  \
}
#define DBOUT_MAT(name,mat) \
		DBOUT(std::endl <<"\"" << name <<"\"" << std::endl << mat << std::endl)
#define DBOUT_START_BLOCK(name) \
		DBOUT(name << std::endl << "----------------------------------------------------------------------" << std::endl)
#define DBOUT_END_BLOCK \
		DBOUT("----------------------------------------------------------------------" << std::endl << std::endl)
#else
#define DBOUT( s )
#define DBOUT_MAT(name,mat)
#define DBOUT_START_BLOCK(name)
#define DBOUT_END_BLOCK
#endif


std::string type2str(int type);



	//
	//OutputDebugStringW(wsTmp.str().c_str( ));  \



// TODO: reference additional headers your program requires here
